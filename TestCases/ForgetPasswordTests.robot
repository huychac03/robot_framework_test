
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
*** Settings ***
Documentation       Tests to validate Forgot Your Password Page functionality
Library     SeleniumLibrary
Test Setup      Open the Browser with URL
Test Teardown   Close Browser Session
Resource       ../Resources/GenericResources.robot
Resource       ../Resources/LoginResources.robot
Resource      ../Resources/ForgetPasswordResources.robot
 
 
*** Test Cases ***
 
Validate Reset Password Functionality
 
    LoginResources.Go to Forgot Your Password Page
    ForgetPasswordResources.Verify Forgot Your Password Page opens
    ForgetPasswordResources.Fill the Forgot Password Page
    ForgetPasswordResources.Verify the message
 
Validate Cancel Functionality
 
    LoginResources.Go to Forgot Your Password Page
    ForgetPasswordResources.Verify Forgot Your Password Page opens
    ForgetPasswordResources.Cancel the Reset Password
    ForgetPasswordResources.Verify that Login Page is displayed
